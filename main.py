from settings import *

import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from heightmapdata import *
from gridpoint import *
import layers
from matplotlib import pylab as plt

scale = get_scale(HEIGHTDATA_FILE)
water_level = get_waterlevel(HEIGHTDATA_FILE)

# Load raw heightmap
hm_raw = np.fromfile(HEIGHTMAP_FILE, dtype='uint16', sep="").reshape([HEIGHTMAP_SIZE[0], HEIGHTMAP_SIZE[1]])

# Convert to meters
hm = np.add(np.multiply(hm_raw, scale), -water_level)

# Flip vertically
hm = np.flipud(hm)

# Matrix debug
# plt.imshow(hm)
# plt.colorbar()
# plt.show()

# Scale up to working size
scaler = np.ones((SCALE[0], SCALE[1]))
hm = np.kron(hm, scaler)

# Make grid
grid = []
for y in range(0, GRID_STEPS[1]):
	for x in range(0, GRID_STEPS[0]):
		sub_start = (GRID_OFFSET[0] + x * GRID_SIZE[0], GRID_OFFSET[1] + y * GRID_SIZE[1])
		sub_end = (sub_start[0] + GRID_SIZE[0], sub_start[1] + GRID_SIZE[1])

		grid.append(GridPoint(sub_start, sub_end))

grid_blocks = layers.height_quantized(hm, grid)
grid_blocks.show()

grid_text = layers.height_text(ImageFont.truetype(FONT_FILE, 25), hm, grid)
grid_text.show()








############### old stuff
from sys import exit
exit()



# Calculate mortar grid
hm_tiled = np.copy(hm_raw)

img_texts = Image.new('RGBA', (OUTPUT_SIZE[0], OUTPUT_SIZE[1]), (0, 0, 0, 256))
draw = ImageDraw.Draw(img_texts)
font = ImageFont.truetype(FONT_FILE, FONT_SIZE)

for y in range(0, GRID_STEPS[1]):
	for x in range(0, GRID_STEPS[0]):
		sub_start = (GRID_OFFSET[0] + x * GRID_SIZE[0], GRID_OFFSET[1] + y * GRID_SIZE[1])
		sub_end = (sub_start[0] + GRID_SIZE[0], sub_start[1] + GRID_SIZE[1])

		# Image
		sub = hm_raw[sub_start[0]:sub_end[0], sub_start[1]:sub_end[1]]
		mean = np.mean(np.mean(sub))

		hm_tiled[sub_start[0]:sub_end[0], sub_start[1]:sub_end[1]] = np.ones([GRID_SIZE[0], GRID_SIZE[1]]) * mean

		# Text





# Flip upside down
# hm_raw = np.flipud(hm_raw)
# hm_tiled = np.flipud(hm_tiled)


# Switching to images
# img_raw = Image.fromarray(hm_raw / 256)
# img_tiled = Image.fromarray(hm_tiled / 256)

# Image debug
# img_tiled.show()
