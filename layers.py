from settings import *

import numpy as np
from gridpoint import *
from heightmapdata import *
from prutil import *
from typing import List, Union
from PIL import Image, ImageDraw, ImageFont

def height_quantized(heightmap: Union[np.array, np.ndarray],
					 grid: List[GridPoint],
					 heights=None,
					 z_resolution: int=10) -> Image:

	if heights is None:
		heights = []
		for grid_point in grid:

			sub = heightmap[grid_point.top:grid_point.bottom, grid_point.left:grid_point.right]
			mean = max(np.mean(np.mean(sub)), 0)

			heights.append(round_to(mean, z_resolution))

	maxh = max(heights)
	minh = min(heights)

	# Crate blank image and draw
	img_blocks = Image.new('RGBA', heightmap.shape, (0, 0, 0, 256))
	draw = ImageDraw.Draw(img_blocks)

	for i in range(0, len(grid)):
		x0 = grid[i].left
		y0 = grid[i].top
		x1 = grid[i].right - 1
		y1 = grid[i].bottom - 1

		val = round((heights[i] - minh) * 100 / maxh)
		fillcolor = 'hsl(0, 0%, {0}%)'.format(val)
		draw.rectangle([x0, y0, x1, y1], outline=None, fill=fillcolor) # Coordinates inverted!

	return img_blocks

def height_text(font: ImageFont,
				heightmap: Union[np.array, np.ndarray],
				grid: List[GridPoint],
				heights=None,
				z_resolution: int=10,) -> Image:

	if heights is None:
		heights = []
		for grid_point in grid:

			sub = heightmap[grid_point.top:grid_point.bottom, grid_point.left:grid_point.right]
			mean = max(np.mean(np.mean(sub)), 0)

			heights.append(round_to(mean, z_resolution))

	# Crate blank image and draw
	img_texts = Image.new('RGBA', heightmap.shape, (0, 0, 0, 256))
	draw = ImageDraw.Draw(img_texts)

	for i in range(0, len(grid)):
		text = str(heights[i])
		t_width, t_height = draw.textsize(text, font=font)
		x, y = grid[i].center()
		x -= t_height / 2
		y -= t_width / 2

		draw.text((y, x), text, font=font) # Coordinates inverted!

	return img_texts

def grid(grid: List[GridPoint]) -> Image:

	pass