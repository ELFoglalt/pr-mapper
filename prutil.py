def raw_to_meters(val, scale, water_level=0):
	return val * scale - water_level

def round_to(x, base=5):
	return int(base * round(float(x)/base))
