class GridPoint:
	def __init__(self, p1, p2):
		self.top = min(p1[0], p2[0])
		self.bottom = max(p1[0], p2[0])
		self.left = min(p1[1], p2[1])
		self.right = max(p1[1], p2[1])

	def __repr__(self):
		return "Grid point from ({0},{1}) to ({2},{3})".format(self.top, self.left, self.bottom, self.right)

	def __str__(self):
		return "Grid point from ({0},{1}) to ({2},{3})".format(self.top, self.left, self.bottom, self.right)

	def width(self):
		return self.right - self.left

	def height(self):
		return self.bottom - self.top

	def center(self):
		return (self.top + self.bottom) / 2, (self.left + self.right) / 2
