import re

def get_scale(filename: str) -> float:
	with open(filename) as datafile:
		contents = datafile.read()

		scale_match = r"--- primary ---.*?\/([0-9]*[\,\.]{0,1}[0-9]*)\/"
		match = re.search(scale_match, contents, flags=re.DOTALL)
		scale = match.group(1)

	return float(scale)

def get_waterlevel(filename: str) -> float:
	with open(filename) as datafile:
		contents = datafile.read()

		water_match = r"setSeaWaterLevel ([0-9]*[\,\.]{0,1}[0-9]*)$"
		match = re.search(water_match, contents, flags=0)
		seawaterlevel = match.group(1)

	return float(seawaterlevel)
