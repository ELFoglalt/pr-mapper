from math import floor

INPUT_DIRECTORY = "./input/"
HEIGHTMAP_FILE = INPUT_DIRECTORY + "heightmapprimary.raw"
HEIGHTDATA_FILE = INPUT_DIRECTORY + "heightdata.con"

HEIGHTMAP_SIZE = (1025, 1025)
OUTPUT_SIZE = (2050, 2050)
SCALE = (floor(OUTPUT_SIZE[0] / HEIGHTMAP_SIZE[0]), floor(OUTPUT_SIZE[1] / HEIGHTMAP_SIZE[1]))

GRID_OFFSET = (floor(SCALE[0] * 24), floor(SCALE[1] * 26))
GRID_SIZE = (floor(SCALE[0] * 25), floor(SCALE[1] * 25))

GRID_STEPS = ( floor((OUTPUT_SIZE[0] - GRID_OFFSET[0]) / GRID_SIZE[0]) - (GRID_OFFSET[0] <= GRID_SIZE[0]),
			   floor((OUTPUT_SIZE[1] - GRID_OFFSET[1]) / GRID_SIZE[1]) - (GRID_OFFSET[1] <= GRID_SIZE[1]) )


FONT_FILE = "./resources/fonts/HelveticaNeueBd.ttf"
FONT_SIZE = 9